FROM ubuntu:22.04
MAINTAINER Giuseppe Paterno' <gpaterno@gpaterno.com>

RUN TZ=Europe/Rome && ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN DEBIAN_FRONTEND=noninteractive apt-get update && \
    apt-get -y upgrade &&  \
    apt-get install -y gnupg2 pass apt-utils openssh-client openssh-server net-tools iputils-ping curl wget

## Get Minio
RUN curl -LO https://dl.min.io/client/mc/release/linux-amd64/mc && mv mc /usr/local/bin && chmod 0755 /usr/local/bin/mc

# Set environment variables.
ENV HOME /root

# Define working directory.
WORKDIR /root

# Define default command.
CMD ["bash"]
